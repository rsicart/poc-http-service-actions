#!/usr/bin/env python3

import http.server
import logging

class MyHTTPServer(http.server.HTTPServer):
    def service_actions(self):
        print('service actions from {}'.format(self.__class__.__name__))


if __name__ == '__main__':

    # setup logger
    log_level = logging.DEBUG
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(level=log_level, format=FORMAT)
    logger = logging.getLogger(__name__)

    # httpd server
    server_address = ('', 8000)
    httpd = MyHTTPServer(server_address, http.server.BaseHTTPRequestHandler)
    logger.info("Serving at port {}".format(server_address[1]))
    httpd.serve_forever()
